Infrastructure
==============

# 1. Goal

The purpose of this repository is to have recipe and provising to setup an entire infrastructure in few minutes (yes I'm hopefull) !
For this I use [Ansible](https://www.ansible.com) as a provisining tool and [Docker](https://www.docker.com/) with [Swarm](https://docs.docker.com/engine/swarm/) (and [Portainer](https://portainer.io/)) to manage application.
I also use [Vagrant](https://www.vagrantup.com) to test recipe.

# 2. Installation

First you need to install [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
Then use file example `hosts.example` and `vars.example.yml` and configuration to your case.
